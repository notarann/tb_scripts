#!/usr/bin/env python

# this fils stores several common variables and functions that are used throughout the other analysis scripts

#1;95;0c Set up (Py)ROOT.
import ROOT
import math
import pickle

ADCtoCharge = 0.00244 # ADC integral to pC charge: 0.122 mV/ADC * 10^-9 sec / 50 ohm
sigma_window = 4 # sets the axis range around the integral histogram 


# load in dictionary used for normilizing calo channels
Calo_ch_normilization_Dict = pickle.load( open( "Calo_ch_normilization_Dict.p", "rb" ) )


# define crystal ball function
def crystalball_function(x, par):
  mean = par[0]
  sigma = par[1]
  alpha = par[2]
  n = par[3]
  N = par[4]
  # evaluate the crystal ball function
  if sigma < 0. :
    return 0.
  z = (x[0] - mean)/sigma
  if (alpha < 0):
    z = -z
  abs_alpha = abs(alpha)
  #C = n/abs_alpha * 1./(n-1.) * math.exp(-alpha*alpha/2.);
  #D = math.sqrt(math.pi/2.)*(1.+math.erf(abs_alpha/math.sqrt(2.)));
  #N = 1./(sigma*(C+D));
  if (z  > - abs_alpha):
    return N * math.exp(- 0.5 * z * z)
  elif (z  < - abs_alpha):
    #double A = std::pow(n/abs_alpha,n) * std::exp(-0.5*abs_alpha*abs_alpha);
    nDivAlpha = n/abs_alpha
    AA =  math.exp(-0.5*abs_alpha*abs_alpha);
    B = nDivAlpha -abs_alpha;
    arg = nDivAlpha/(B-z);
    return N * AA * (abs(arg)**n)
# Create a ROOT function for the crystalball defined above
crysball = ROOT.TF1("crystalball_function",crystalball_function,0,500000,5)


# Define a N/sqrt(E) function to fit resolution graph
def inverseroot_function(x, par):
  N = par[0]
  C = par[1]
  return (C**2 + N**2/x[0])**(1/2)
# Create a ROOT function for inverseroot defined above
inverseroot = ROOT.TF1("inverseroot_function",inverseroot_function,0.01,300,2)
inverseroot.SetParameters(0.1,0.02)
inverseroot.SetParLimits(0,0.05,0.5)
inverseroot.SetParLimits(1,0.0,0.05)
inverseroot.SetParNames("N","C")


def fitNormalizedCalo(hist):
    gaus0 = ROOT.TF1("gaus0", "gaus", hist.GetMean(), hist.GetMean()+1.5*hist.GetStdDev())
    hist.Fit("gaus0","MR") # first fit histogram with gaussian that finds rough gaussian mean and width
    hist.Fit("gaus", "M+","", hist.GetFunction("gaus0").GetParameter(1)-0.8*hist.GetFunction("gaus0").GetParameter(2), hist.GetFunction("gaus0").GetParameter(1) + 3*hist.GetFunction("gaus0").GetParameter(2)) # fit again with a second but using the first gaussian to limit the range of the second gaussian
    hist.GetFunction("gaus").SetLineColor(3)
    hist.GetFunction("gaus0").SetLineColor(6)

    # Sets initial fit values and parameter names
    crysball.SetParameters(hist.GetFunction("gaus").GetParameter(1),hist.GetFunction("gaus").GetParameter(2),1.0,100.0,hist.GetFunction("gaus").GetParameter(0))
    crysball.SetParLimits(0,hist.GetFunction("gaus").GetParameter(1),hist.GetFunction("gaus").GetParameter(1))
    crysball.SetParLimits(1,hist.GetFunction("gaus").GetParameter(2),hist.GetFunction("gaus").GetParameter(2))
    crysball.SetParLimits(2,0.2,4.0)
    crysball.SetParLimits(3,100.0,100.0)
    crysball.SetParLimits(4,hist.GetFunction("gaus").GetParameter(0),hist.GetFunction("gaus").GetParameter(0))
    crysball.SetParNames("Mean","Sigma","alpha","n","Constant")

    hist.Fit("crystalball_function", "M+","", hist.GetFunction("gaus").GetParameter(1)-sigma_window*hist.GetFunction("gaus").GetParameter(2),hist.GetFunction("gaus").GetParameter(1)+sigma_window*hist.GetFunction("gaus").GetParameter(2))


# only process good events: ones that have a single track in the tracker, <= 10 clusters, and that track centers on current point
def event_selection(tree, Xcenter, Ycenter):
    #if not (2=2): return False
    if not tree.FaserTriggerData.tap() & 0x04: return False
    elif tree.ClusterFit.m_tracks.size() != 1: return False
    elif tree.SCT_ClusterContainer.m_rawdata.size() > 10: return False
    elif abs(tree.ClusterFit.m_parameters[0].m_parameters[0] - Xcenter) > 20: return False
    elif abs(tree.ClusterFit.m_parameters[0].m_parameters[1] - Ycenter) > 20: return False
    else: return True
    #return True  


def logg2E(x, par):
 Z = par[0] 
 P = par[1] 
 return ((math.log((Z)*x[0]))*P)
# Create a ROOT function for inverseroot defined above
Log2E = ROOT.TF1("Log2E",logg2E,5,300,2)
Log2E.SetParameter(0, 0.1)
Log2E.SetParameter(1, 0.1)
Log2E.SetParLimits(0,0.1,300)
Log2E.SetParLimits(1,0.1,300)
Log2E.SetParNames("Z", "P")
