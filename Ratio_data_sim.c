
void Ratio_data_sim() {
  // Define two gaussian histograms. Note the X and Y title are defined
  // at booking time using the convention "Hist_title ; X_title ; Y_title"
  TFile *_file1 = TFile::Open("Elec_Sim1_3583.root");  //sim
  TFile *_file2 = TFile::Open("test3583_1.root"); //data

  TH1D *h1=(TH1D*)_file1->Get("Pre1_eLossTot");   
  //TH1D *h2=(TH1D*)_file2->Get("Pre0_eLossTot");
  TH1D *h2=(TH1D*)_file2->Get("Total_E_in_PS_ch6");
  // Define the Canvas
  TCanvas *c = new TCanvas("c", "canvas", 800, 800);
  
  // Upper plot will be in pad1
  TPad *pad1 = new TPad("pad1", "pad1", 0, 0.3, 1, 1.0);
  pad1->SetBottomMargin(0); // Upper and lower plot are joined
  pad1->SetGridx();         // Vertical grid
  pad1->Draw();             // Draw the upper pad: pad1
  pad1->cd();               // pad1 becomes the current pad
  
  h1->GetXaxis()->SetRangeUser(0.0, 2.0);
  h1->GetYaxis()->SetRangeUser(0.0, 450.0);  
  h2->Scale(1/h2->Integral());   
  h1->Scale(1/h1->Integral());
  h2->SetMarkerStyle(21);
  h2->SetMarkerSize(0.5);
  
                                                                                                                           
                                                                                                                         
  h1->SetStats(0);
  h2->SetStats(0);
  h1->Draw("HIST");               // Draw h1
  h2->Draw("same");         // Draw h2 on top of h1
  auto legend = new TLegend(0.1,0.7,0.48,0.9);

  legend->SetHeader("The Legend","C"); // option "C" allows to center the header                                                         

  legend->AddEntry(h1,"Sim run 3583, point8, 100GeV e","l");

  legend->AddEntry(h2,"Data run 3583, point8, run# 3583, 100GeV e","lp");
  legend->Draw();
  // Do not draw the Y axis label on the upper plot and redraw a small
  // axis instead, in order to avoid the first label (0) to be clipped.
 

  // lower plot will be in pad
  c->cd();          // Go back to the main canvas before defining pad2
  TPad *pad2 = new TPad("pad2", "pad2", 0, 0.05, 1, 0.3);
  pad2->SetTopMargin(0);
  pad2->SetBottomMargin(0.2);
  pad2->SetGridx(); // vertical grid
  pad2->Draw();
  pad2->cd();       // pad2 becomes the current pad

  // Define the ratio plot
  TH1F *h3 = (TH1F*)h2->Clone();
  h3->SetLineColor(kBlack);
  h3->SetMinimum(0.0);  // Define Y ..
  h3->SetMaximum(2.0); // .. range
  //  h3->Sumw2();
  h3->SetStats(0);
        // No statistics on lower plot
  h3->Divide(h1);
  h3->SetMarkerStyle(1);
  //h3->GetXaxis()->SetRangeUser(0.0, 1.5);
  h3->Draw("ep");       // Draw the ratio plot

  // h1 settings
  h1->SetLineColor(kBlue+1);
  h1->SetLineWidth(2);

  // Y axis h1 plot settings
  h1->GetYaxis()->SetTitleSize(20);
  h1->GetYaxis()->SetTitleFont(43);
  h1->GetYaxis()->SetTitleOffset(1.55);

  // h2 settings
  h2->SetLineColor(kRed);
  h2->SetLineWidth(2);

  // Ratio plot (h3) settings
  h3->SetTitle(""); // Remove the ratio title

  // Y axis ratio plot settings
  h3->GetYaxis()->SetTitle("ratio h1/h2 ");
  h3->GetXaxis()->SetRangeUser(0.0, 2.0);
  h3->GetYaxis()->SetNdivisions(505);
  h3->GetYaxis()->SetTitleSize(20);
  h3->GetYaxis()->SetTitleFont(43);
  h3->GetYaxis()->SetTitleOffset(1.55);
  h3->GetYaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
  h3->GetYaxis()->SetLabelSize(15);

  // X axis ratio plot settings
  h3->GetXaxis()->SetTitleSize(20);
  h3->GetXaxis()->SetTitleFont(43);
  h3->GetXaxis()->SetTitleOffset(4.);
  h3->GetXaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
  h3->GetXaxis()->SetLabelSize(15);
  
  //auto legend = new TLegend(0.1,0.7,0.48,0.9);

  //legend->SetHeader("The Legend","C"); // option "C" allows to center the header                                                 
                                                                                                                                      

  //legend->AddEntry(h3,"Electrons:30GeV, point8","l");                                                                               
  //legend->AddEntry(h2,"Electrons:100GeV, point 8","l");                                                                             
  //legend->AddEntry(h1,"Sim run 3575, point8, 100GeV e","l");                                                                                
  //legend->AddEntry(h2,"Data run 3575, point8, run# 3575, 100GeV e","lp");
  //legend->Draw();


}
