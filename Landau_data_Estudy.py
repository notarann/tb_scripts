                                                                         
#!/usr/bin/env python

# Set up (Py)ROOT.
import ROOT
import datetime
from array import array
import math
import datetime
import pickle


from TestBeamCommon import crystalball_function, event_selection

# Initialize the xAOD infrastructure:
if(not ROOT.xAOD.Init().isSuccess()): print("Failed xAOD.Init()")



pick_ch = 6
inputrun = str(input("enter run number to analyze: "))
# Use Tchains to load in multiple root files and combine them into a single Ttree
t = ROOT.TChain("CollectionTree")

#nfiles = t1.Add("/eos/project-f/faser-commissioning/Reconstruction/Run-003430/Faser-Physics-003430-00000.xAOD.root") #nfiles then has the number of files read in
#nfiles = t1.Add("/eos/user/n/notarann/digi_new/run/python/WaveRecAlgs/SimToRec.xAOD.root")
#nfiles = t.Add("/eos/project-f/faser-commissioning/Reconstruction/Run-003583/Faser-Physics-003583-00000.xAOD.root, /eos/project-f/faser-commissioning/Reconstruction/Run-003583/Faser-Physics-003583-00000-r0005-xAOD.root")
nfiles = t.Add("/eos/project-f/faser-commissioning/Reconstruction/Run-003583/Faser-Physics*.xAOD.root")
txAOD = ROOT.xAOD.MakeTransientTree(t,2)

print("number of files combined using TChain = ", nfiles)


PreshowerWaveformHits_Available = txAOD.GetBranchStatus("PreshowerWaveformHits")


print("Preshower Waveform data is avaliable: ", PreshowerWaveformHits_Available )

txAOD.GetEntry(0)
#print("test run number get = ", t.EventInfo.runNumber())

txAOD.Print()
#t.Scan()
#t.Show()

ROOT.gROOT.SetStyle("ATLAS")
ROOT.gStyle.SetOptStat(1111) #take away option box in histograms
ROOT.gStyle.SetOptTitle(1)
ROOT.gStyle.SetOptFit(111)


filename = "Elecclean_ch6_"+inputrun+"GeV"+".pdf"

c = ROOT.TCanvas()
c.Print(filename+'[')  
header = ROOT.TPaveText(0.05,0.1,0.1,0.1, option = "TL")
header.SetTextSize(0.03)
header.SetFillColor(0)
header.SetTextAlign(22)
header.AddText("FASER")

Ngoodevents = 0
i = 0
x= array( 'f' )
n = 0
print( "Number of input events: %s" % txAOD.GetEntries() )
# Now just loop over all events
for i in range(t.GetEntries()):  
    t.GetEntry(i)
    txAOD.GetEntry(i) 

    if i%1000 == 0:
        
       print( " Event #%i, Good events #%i, N #%i" % ( i, Ngoodevents, n) )
    

    if i == t.GetEntries()-1: #end loop right before last event as it is empty and causes seg faults
            break
   
    if event_selection(t, 0.0, 20.0) == False: continue  #cuts to get good events, the function is defined in testbeamcommon.py
    Ngoodevents += 1
    n += 1

    #PS_total=0
    if txAOD.GetBranchStatus("PreshowerWaveformHits"):
       for PS in txAOD.PreshowerWaveformHits:
           if PS.channel() == 6: # pick channel 0
#                print (det.channel())
                if PS.status() == 0 or PS.status() == 2 or PS.status() == 4: # if hit is above threshold: status == 0, if multiple hits: status == 2, and if it is overflow: status == 4                   
                                                                            
#                       print(det.integral())
                       x.append(float(PS.integral()))  
                       
        
                    
    i+=1
     

    
                       

    # End of loop over particles
graph = ROOT.TH1F("Total_ADC_in_PS_ch6", "Total ADC in PS (first layer)", 10000, 0, 120000)
graph1 = ROOT.TH1F("Total_Q_in_PS_ch6", "Total Q in PS (first layer)", 1000, 0, 350)
graph2 = ROOT.TH1F("Total_E_in_PS_ch6", "Total E in PS (first layer)", 1000000, 0, 1000)                              


v = len(x)
for loss in range(v):                                                                                         
  graph.Fill(x[loss])
graph.GetXaxis().SetTitle( 'ADC' )                      
graph.GetXaxis().SetRangeUser( 5.0,120000.0 )
graph.GetYaxis().SetTitle( 'Events' )                       
graph.SetTitle( 'ch'+str(pick_ch) )                             
c = ROOT.TCanvas()
graph.Rebin(10)
graph.GetXaxis().SetRangeUser( 5.0,120000.0 )    
graph.Draw()
c.Print(filename)
c.Clear()

for loss in range(v):                                                                                         
  graph1.Fill(x[loss]*0.00244)  
graph1.GetXaxis().SetTitle( 'pC' )                                                                   
graph1.GetYaxis().SetTitle( 'Events' )
graph1.SetTitle( 'ch'+str(pick_ch) )
c = ROOT.TCanvas()
#graph1.Rebin(10)
graph1.GetXaxis().SetRangeUser( 0.0,400.0 )
#graph1.Fit("landau")                                                                                                
#graph1.GetFunction("landau").SetLineColor(3)
graph1.Draw() 
c.Print(filename)
c.Clear()

for loss in range(v):
  graph2.Fill(x[loss]*0.00244*0.00007723)
graph2.GetXaxis().SetTitle( 'GeV' )
graph2.GetYaxis().SetTitle( 'Events' )
graph2.SetTitle( 'ch'+str(pick_ch) )
graph2.GetXaxis().SetRangeUser(10.0,350)  
c = ROOT.TCanvas()
#graph2.Rebin(5)
graph2.GetXaxis().SetRangeUser( 0.002,0.05 )  
graph2.Draw()
c.Print(filename)
c.Clear()

f=ROOT.TFile("test3583_1.root","recreate")
#graph.Write()
graph2.Write()
f.Write()
f.Close()
# End of loop over events
print()

cend = ROOT.TCanvas()

# Must close file at the end
cend.Print(filename+']')



