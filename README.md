# TB_scripts

Scripts for processing data and simulation files for the FASER TB analysis 2021/22:


Landau_data.py: this script is to process the TB data files primarily done to do fits for the Muon scans.


Landau_data_Estudy.py: this script is to process the TB data files and to convert them into charge and then into GeV.


roothistLandau_SIM.c: this script is there to process the MC files and to do landau fits.


Ratio_data_sim.c: takes the th1 from data and sim and puts them into the same canvas.


TestBeamCommon.py: has all the functions that are defined that are used in the other scripts