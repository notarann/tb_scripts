#include<iostream>
#include"TH1D.h"
#include"TFile.h"
#include"TMath.h"

using namespace std;

void roothistLandau_SIM(){
  //TFile *_file1 = TFile::Open("Elec_30GeV_split1.root");
 
  TH1D *h1=(TH1D*)_file1->Get("Pre0_eLossTot");
  
  TCanvas *c1 = new TCanvas("c1","c1");
  //c1->SetLogy();
  
  //Double_t scale1 = h1->GetXaxis()->GetBinWidth(1)/(h1->Integral());
  

                                                                                                                                                                              
  gStyle->SetOptStat(111111111);
  gStyle->SetOptFit(111);
  h1->GetXaxis()->SetTitle("Preshower Layer Total Edep (GeV) layer 0");
  //h1->Fit("landau");
  //h1->Draw();

  h1->Rebin(10);
  h1->GetXaxis()->SetRangeUser(0.005, 2.0); 
 
  h1->Draw();
  auto legend = new TLegend(0.1,0.7,0.48,0.9);
  legend->SetHeader("The Legend","C"); // option "C" allows to center the header                                                                                                                        
  legend->AddEntry(h1,"Muon 150GeV, point8, run# 3430","l");
  legend->Draw();
  
  TFile f("Muon_3430__.root","recreate");
  f.cd();
  h1->Write();
  f.Write();
  f.Close();


}
